﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookApp.DataAccess
{
    internal interface ICrudRepository <T, ID>
    {
        /// <summary>
        /// Gets all the entities of type T.
        /// </summary>
        /// <returns></returns>
        ICollection<T> GetAll();
        /// <summary>
        /// Gets the entity based on the id of the entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(ID id);

        /// <summary>
        /// Adds the entity to the database and returns number of rows affected
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Add(T entity);
        /// <summary>
        /// Updates the entity in the database and returns number of rows affected
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Update(T entity);

    }
}
