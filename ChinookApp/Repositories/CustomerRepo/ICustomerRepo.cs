﻿using ChinookApp.DataAccess;
using ChinookApp.Models;


namespace ChinookApp.Repositories.CustomerRepo
{
    internal interface ICustomerRepo: ICrudRepository <Customer, int>
    {
        /// <summary>
        /// Gets the customer based of the name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ICollection<Customer> GetByName(string name);

        /// <summary>
        /// Gets a collection of CustomerSpenders that are ordered in decending order based on the total amount spent
        /// </summary>
        /// <returns></returns>
        ICollection<CustomerSpender> GetHighestSpenders();

        /// <summary>
        /// Gets the most popular genre of a specific Customer id, returns multiple if multiple most popular
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        ICollection<CustomerGenre> GetMostPopularGenres(int customerId);

        /// <summary>
        ///  Gets a collection of customers of size "limit" with an offset "offset", if not specified limit and offset
        ///  are defaulted to 0
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        ICollection<Customer> GetCustomerPage(int limit = 0, int offset = 0);

        /// <summary>
        /// Gets collection of number of customers from each country in decending order.
        /// </summary>
        /// <returns></returns>
        ICollection<KeyValuePair<string, int>> GetCustomersPerCountry();

    }
}
