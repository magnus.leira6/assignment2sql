﻿using ChinookApp.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookApp.Repositories.CustomerRepo
{
    internal class CustomerRepoImpl : ICustomerRepo
    {
        private readonly string _connectionString;

        public CustomerRepoImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Add(Customer entity)
        {
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "INSERT INTO Customer (FirstName, LastName, " +
                            "Phone, Email, Country, PostalCode) VALUES " +
                            "(@FirstName, @LastName, @Phone, @Email, @Country, @PostalCode );";
            using SqlCommand cmd = new SqlCommand(query, connection);

            cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@FirstName"].Value = entity.FirstName;
            cmd.Parameters.Add("@LastName", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@LastName"].Value = entity.LastName;
            cmd.Parameters.Add("@Phone", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@Phone"].Value = entity.Phone;
            cmd.Parameters.Add("@Email", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@Email"].Value = entity.Email;
            cmd.Parameters.Add("@Country", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@Country"].Value = entity.CustomerCountry.Country;
            cmd.Parameters.Add("@PostalCode", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@PostalCode"].Value = entity.CustomerCountry.PostalCode;
            int rowsAffected = cmd.ExecuteNonQuery();
            return rowsAffected;
           
        }

        public ICollection<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "SELECT * FROM Customer;";
            using SqlCommand cmd = new SqlCommand(query, connection);   
            using SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                 Customer customer = new Customer(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    DBNull.Value == reader[9] ? null : reader.GetString(9),
                    reader.GetString(11),
                    new CustomerCountry(
                         DBNull.Value == reader[7] ? null : reader.GetString(7),
                         DBNull.Value == reader[8] ? null : reader.GetString(8)
                ));
                customers.Add(customer);
                
            }
            return customers;
        }

        public Customer GetById(int id)
        {
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "SELECT * FROM Customer WHERE Customer.CustomerId = @Id;";
            using SqlCommand cmd = new SqlCommand(query, connection);
            cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
            cmd.Parameters["@Id"].Value = id;
            using SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            return new Customer(
                   reader.GetInt32(0),
                   reader.GetString(1),
                   reader.GetString(2),
                   DBNull.Value == reader[9] ? null : reader.GetString(9),
                   reader.GetString(11),
                   new CustomerCountry(
                         DBNull.Value == reader[7] ? null : reader.GetString(7),
                         DBNull.Value == reader[8] ? null : reader.GetString(8)
               ));

        }

        public ICollection<Customer> GetByName(string name)
        {
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "SELECT * FROM Customer AS c WHERE c.FirstName LIKE " + "@Name" + ";";
            using SqlCommand cmd = new SqlCommand(query, connection);
            cmd.Parameters.Add("@Name", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@Name"].Value = name + "%";
            using SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Customer customer = new Customer(
                   reader.GetInt32(0),
                   reader.GetString(1),
                   reader.GetString(2),
                   DBNull.Value == reader[9] ? null : reader.GetString(9),
                   reader.GetString(11),
                   new CustomerCountry(
                         DBNull.Value == reader[7] ? null : reader.GetString(7),
                         DBNull.Value == reader[8] ? null : reader.GetString(8)
               ));
                customers.Add(customer);

            }
            return customers;
        }
        
        public ICollection<Customer> GetCustomerPage(int limit = 0 , int offset = 0)
        {
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "SELECT * FROM Customer AS c " +
                           "ORDER BY c.CustomerId " +
                           "OFFSET @Offset ROWS " +
                           "FETCH NEXT @Limit ROWS ONLY ";
            using SqlCommand cmd = new SqlCommand(query, connection);
            cmd.Parameters.Add("@Limit", System.Data.SqlDbType.Int);
            cmd.Parameters["@Limit"].Value = limit;
            cmd.Parameters.Add("@Offset", System.Data.SqlDbType.Int);
            cmd.Parameters["@Offset"].Value = offset;

            using SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Customer customer = new Customer(
                   reader.GetInt32(0),
                   reader.GetString(1),
                   reader.GetString(2),
                   DBNull.Value == reader[9] ? null : reader.GetString(9),
                   reader.GetString(11),
                   new CustomerCountry(
                         DBNull.Value == reader[7] ? null : reader.GetString(7),
                         DBNull.Value == reader[8] ? null : reader.GetString(8)
               ));
                customers.Add(customer);

            }
            return customers;
        }
        public ICollection<KeyValuePair<string, int>> GetCustomersPerCountry()
        {
            List<KeyValuePair<string, int>> countries = new List<KeyValuePair<string, int>>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "SELECT Country, " +
                        "COUNT(*) Customers FROM Customer " +
                        "GROUP BY Country ORDER BY Customers DESC;";

            using SqlCommand cmd = new SqlCommand(query, connection);
            using SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                countries.Add(
                    new KeyValuePair<string, int>(reader.GetString(0), reader.GetInt32(1))
                    );
            }
            return countries;
        }

        public ICollection<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "SELECT c.CustomerId, i.InvoiceId, MAX(i.Total) HighestSpending " +
                "FROM Customer AS c LEFT JOIN Invoice AS i ON i.CustomerId = c.CustomerId " +
                "GROUP BY c.CustomerId, i.InvoiceId ORDER BY HighestSpending DESC;";

            using SqlCommand cmd = new SqlCommand(query, connection);
            using SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                CustomerSpender spender = new CustomerSpender(
                    reader.GetInt32(0),
                    DBNull.Value == reader[2] ? null : reader.GetInt32(1),
                    DBNull.Value == reader[2] ? null : (double) reader.GetDecimal(2)
                    );
                customerSpenders.Add(spender);
            }

            return customerSpenders;
        }

        public ICollection<CustomerGenre> GetMostPopularGenres(int customerId)
        {
            List<CustomerGenre> genres = new List<CustomerGenre>();

            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "SELECT TOP 1 WITH TIES " +
                "c.CustomerId, g.Name FROM Customer AS c " +
                "LEFT JOIN Invoice AS i ON c.CustomerId = i.CustomerId " +
                "LEFT JOIN InvoiceLine AS il ON i.InvoiceId = il.InvoiceId " +
                "LEFT JOIN Track AS t ON il.TrackId = t.TrackId " +
                "LEFT JOIN Genre AS g ON g.GenreId = t.GenreId " +
                "WHERE c.CustomerId = @Id GROUP BY c.CustomerId, g.Name " +
                "ORDER BY COUNT(g.Name) DESC;";
            using SqlCommand cmd = new SqlCommand(query, connection);
            cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
            cmd.Parameters["@Id"].Value = customerId;
            using SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                CustomerGenre genre = new CustomerGenre(
                        reader.GetInt32(0),
                        reader.GetString(1)
                );
                genres.Add(genre);
            }

            return genres;
        }

        public int Update(Customer entity)
        {
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string query = "UPDATE Customer SET " +
                "FirstName = @FirstName, LastName = @LastName, Phone = @Phone," +
                "Email = @Email, Country = @Country, PostalCode = @PostalCode " +
                "WHERE Customer.CustomerId = @Id;" ;
            using SqlCommand cmd = new SqlCommand(query, connection);

            cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
            cmd.Parameters["@Id"].Value = entity.Id;
            cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@FirstName"].Value = entity.FirstName;
            cmd.Parameters.Add("@LastName", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@LastName"].Value = entity.LastName;
            cmd.Parameters.Add("@Phone", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@Phone"].Value = entity.Phone;
            cmd.Parameters.Add("@Email", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@Email"].Value = entity.Email;
            cmd.Parameters.Add("@Country", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@Country"].Value = entity.CustomerCountry.Country;
            cmd.Parameters.Add("@PostalCode", System.Data.SqlDbType.NVarChar);
            cmd.Parameters["@PostalCode"].Value = entity.CustomerCountry.PostalCode;
            int rowsAffected = cmd.ExecuteNonQuery();
            return rowsAffected; 
        }
    }
}
