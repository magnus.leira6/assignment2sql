﻿using ChinookApp.Models;
using ChinookApp.Repositories.CustomerRepo;
using ChinookApp.Utils;

namespace ChinookApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepo customerRepo = new CustomerRepoImpl(DbConUtil.GetConnectionString());

            // 1.
            //ICollection<Customer> customers = customerRepo.GetAll();
            //customers.ToList().ForEach(c => Console.WriteLine(c));

            // 2.
            //Console.WriteLine(customerRepo.GetById(9));

            // 3.
            //ICollection<Customer> customers = customerRepo.GetByName("Ed");
            //customers.ToList().ForEach(c => Console.WriteLine(c));

            // 4.
            //ICollection<Customer> customers = customerRepo.GetCustomerPage(15,25);
            //customers.ToList().ForEach(c => Console.WriteLine(c));

            // 5.
            /*Customer customer = new Customer(111, "Michal", "Kowalski", "1994983843", "kowalski@statusreport.com",
                                                new CustomerCountry("Norway", "1353"));
            int rowsAffected = customerRepo.Add(customer);
            Console.WriteLine("Rows affected: " + rowsAffected);*/

            // 6.
            /*Customer customer = new Customer(60, "Michal", "Kowalski", "1994983843", "kowalski@statusreport.com",
                                                new CustomerCountry("Norway", "0284"));
            int rowsAffected = customerRepo.Update(customer);
            Console.WriteLine("Rows affected: " + rowsAffected);*/

            // 7.
            /*List<KeyValuePair<string,int>> countries = customerRepo.GetCustomersPerCountry().ToList();
             countries.ForEach(c => Console.WriteLine(c));*/

            // 8. 
            /*List<CustomerSpender> spenders = customerRepo.GetHighestSpenders().ToList();
            spenders.ForEach(c => Console.WriteLine(c)); */

            // 9.
            List<CustomerGenre> genres = customerRepo.GetMostPopularGenres(12).ToList();
            genres.ForEach(c => Console.WriteLine(c));

        }
    }
}