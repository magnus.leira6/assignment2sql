﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookApp.Models
{
    internal record struct CustomerGenre(int Id, string Name);
}
