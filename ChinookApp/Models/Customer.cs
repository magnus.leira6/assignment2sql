﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookApp.Models
{
    internal record struct Customer(int Id, string FirstName, string LastName, string? Phone, string Email,
                CustomerCountry CustomerCountry);
}
