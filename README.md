# Chinook

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

C# .NET project delivery for the assignment 2 module at Noroff. It reproduces the functionality requested in the assignment 2 file.

The different records are the main domain of the program. Customer, CustomerCountry, CustomerGenre and CustomerSpender are records representing different values corresponding to a single domain customer. The records encapsulate a chosen set of data, leaving the remaining fields found in the database invisible to the user. All functionality is accessible through a repository implementation, that implements the CRUD and customer interfaces. Therefore any further repository extension will be forced to satisfy required functionality.

Currently, the functionality implemented for Customer object is: GetAll, GetById, Add, Update, GetByName, GetHighestSpenders, GetMostPopularGenres, GetCustomerPage (returns a page of customers and their available data) and GetCustomersPerCountry. All the SQL statements written are being sanitized in order to prevent SQL injection. Most of the methods return either a single Customer object, or a List of Customer objects. 

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Background
The background for this project was a mandatory assignment at Noroff Accelerate.

## Install
To install the project, simply clone the repository and open it up in Visual Studio.

#### Dependencies
.NET6.0

SQLClient

## Usage
In order to run the project, open up the project folder in Visual Studio and uncomment method invokes found in the Program.cs static class.

## Maintainers

[@Xerethars](https://github.com/Xerethars)

[@Magnus](https://github.com/h578031)

## License

MIT © 2022 Magnus & Michal
