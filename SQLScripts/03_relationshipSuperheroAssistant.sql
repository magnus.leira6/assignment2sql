USE SuperheroesDb;
GO

ALTER TABLE Assistant
ADD SuperheroId int CONSTRAINT FK_Assistant FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id) ON DELETE SET NULL;

