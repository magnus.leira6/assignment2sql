USE SuperheroesDb;
GO

CREATE TABLE Superhero (
	Id int identity(1,1) primary key,
	Name varchar(50),
	Alias varchar(50),
	Origin varchar(100),
);

CREATE TABLE Assistant (
	Id int identity(1,1) primary key,
	Name varchar(50),
);

CREATE TABLE Power (
	Id int identity(1,1) primary key,
	Name varchar(50),
	Description varchar(200),
);