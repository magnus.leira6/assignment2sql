USE SuperheroesDb;
GO

CREATE TABLE Superhero_Power (
	SuperheroId int,
	PowerId int,
	PRIMARY KEY (SuperheroId, PowerId),
	FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id),
	FOREIGN KEY (PowerId) REFERENCES Power(Id),
	);