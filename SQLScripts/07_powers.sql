USE SuperheroesDb;
GO

INSERT INTO Power 
VALUES ('Super Strength', '1000 times stronger than the average man'),
	   ('Invicibility', 'No one can see you'),
	   ('Mind Reading', 'Can read anyones thoughts'),
	   ('Flying','Can fly');

INSERT INTO Superhero_Power
VALUES (1, 1),
	   (1, 3),
	   (2, 1),
	   (3,2);